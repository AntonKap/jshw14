
let btn = document.querySelector(".btnSubject");
let bodik = document.querySelector(".body");
let textikp = document.querySelectorAll("p");
let textikh2 = document.querySelector(".h2-title");

function toggleDark() {
    btn.classList.toggle('btnSubjectDark');
    bodik.classList.toggle("bodyAfter");
    textikp.forEach(e => e.classList.toggle("textChange"));
    textikh2.classList.toggle("textChange");
    btn.textContent = btn.textContent === "dark" ? "white" : "dark";
}

btn.addEventListener("click", function() {
    toggleDark();
    localStorage.setItem("isDark", btn.textContent === "white");
});

if (localStorage.getItem("isDark") === "true") {
    toggleDark();
}


